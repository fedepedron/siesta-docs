:sequential_nav: next

..  _building_with_conda:

Installing Siesta with conda
============================

`Conda <https://conda.io>`_ is a package management system that runs on
multiple operating systems and installs pre-compiled versions of packages,
making sure that all packages are compatible with each other compiler-wise.

With ``conda``, installing SIESTA is as simple as:

.. code-block:: bash

  conda install -c conda-forge siesta

This will install the latest serial version of SIESTA.
Installing a parallel vesion is just as easy:

.. code-block:: bash

  # With openmpi
  conda install -c conda-forge "siesta=*=*openmpi*"
  # With mpich
  conda install -c conda-forge "siesta=*=*mpich*"

There are no requirements for the parallel installation, conda will install
everything that is needed for your requested installation.

.. warning::
  Conda provides a pre-compiled package. This means that it will only be available
  for some platforms. This batch shows the current available platforms for SIESTA:

  .. figure:: https://anaconda.org/conda-forge/siesta/badges/platforms.svg
    :target: https://anaconda.org/conda-forge/siesta
    :alt: Platforms

  If your platform is not listed and you would like support for it, please
  submit an issue `here <https://github.com/conda-forge/siesta-feedstock/issues>`_.

.. note::
  If you don't have conda installed, first install it. There are multiple ways
  of installing conda, but we recommend installing it through ``miniforge``:

  https://github.com/conda-forge/miniforge

  which installs ``conda`` and ``mamba`` (a replacement of conda that is faster at
  installing packages).

  Then, it is recommended to create a new environment so that everything that you install
  doesn't mess with the other environments that you may have:

  .. code-block:: bash

    # Create an environment called siesta.
    conda create -n siesta
    # Activate the environment so that we can work on it.
    conda activate siesta


.. warning::
  Anaconda Inc. started to enforce the terms of service for their `defaults` channel
  (see the `-c` option above), regarding its usage in academic institutions.
  Stick to conda-forge or similar channels;  otherwise, your institution might be
  charged for the service. You can directly remove the defaults channel from conda,
  and ensure that you only use conda-forge channels, by doing::

    conda config --remove channels defaults
    conda config --add channels conda-forge

  See:

    * `<https://legal.anaconda.com/policies>`_
    * `<https://www.anaconda.com/pricing/terms-of-service-faqs>`_
    * `<https://www.cdotrends.com/story/4173/anaconda-threatens-legal-action-over-licensing-terms>`_


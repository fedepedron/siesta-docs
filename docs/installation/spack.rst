:sequential_nav: next

..  _building_with_spack:

Building Siesta with spack
==========================

`Spack <https://spack.io>`_ is a package manager targeting research
software and supercomputers, although it proves useful even for
software management on a personal machine. It can handle in a
consistent manner the "software stacks" (think of the compiler, the
MPI libraries, optimized linear-algebra libraries, etc).

Please consult the Spack documentation for installation instructions.

It is possible to install SIESTA with Spack by using appropriate versions
of the "recipes" used by SIESTA and its dependencies. Typically, these recipes
are kept in the `builtin` repository that is part of the Spack distribution.
However, the Siesta-related recipes in this default repository might be outdated. 

The procedure involves telling Spack to overlay a custom recipe repository on top
of the default one. The custom repository contains recipes prepared
by the Siesta Project, and is available in the `build-tools` GitLab repo::

    git clone https://gitlab.com/siesta-project/ecosystem/build-tools.git

Navigate to the `Spack` directory and do::

    spack repo add siesta-project-spack-repo  

If you now ask `spack` about what it knows about package repositories::

    spack repo list

it will answer with something like::

    ==> 2 package repositories.
    siesta-project    /path/to/Spack/siesta-project-spack-repo
    builtin           /path/to/installed/spack/var/spack/repos/builtin

By making sure that the 'siesta-project' repo is listed first in the
spack repository chain we get the right recipes for the build.

.. note:: If you are using a spack installation in a supercomputer
	  center, you might have trouble adding extra repositories or,
	  in general, modifying the spack configuration.
          Please see :ref:`this page <spack_issues>` for more information.

After this configuration step, and asuming that the rest of the Spack
installation has been carried out (for definition of compilers, etc) you
can issue commands such as::

     spack install xmlf90
     spack info siesta
     spack spec siesta +mpi +netcdf +libxc +elpa
     spack install siesta ~mpi build_type=Debug
     spack install siesta +mpi +netcdf +libxc +elpa ^elpa@2024.05.001 +cuda cuda_arch=80

The last two lines will respectively install a serial version of
Siesta ("~mpi" means "no mpi") with debugging symbols, and a parallel
version of Siesta with NetCDF and Libxc support, and with a
CUDA-enabled modern version of the ELPA library.


Available Spack versions of SIESTA
----------------------------------

Please run::

  spack info siesta

and look at the first lines for a list of available versions. At present, those include::

  Preferred version:  
    5.2.0            [git] https://gitlab.com/siesta-project/siesta.git at tag 5.2.0

  Safe versions:  
    develop          [git] https://gitlab.com/siesta-project/siesta.git on branch dev
    5.2-head         [git] https://gitlab.com/siesta-project/siesta.git on branch rel-5.2
    5.2.0            [git] https://gitlab.com/siesta-project/siesta.git at tag 5.2.0
    5.0.2            [git] https://gitlab.com/siesta-project/siesta.git at tag 5.0.2


The "preferred" version is the latest release (5.2.0), and that would
be installed by default, but users can also request versions in the
rel-5.2 branch with fixes over 5.2.0 (e.g. the "tip" 5.2-head), and
the bleeding-edge development version `develop` (this is not
recommended except for tests, as the branch moves fast).

.. note:: The list of versions shown by `spack info siesta` is likely
          to change as you refresh the contents of the `build-tools`
          repository above by pulling from the repo.
          Patched versions and pre-release (e.g. alpha, beta, rc) versions
          might appear on the list to provide more flexibility to
          advanced users and developers. Eventually the recipes will
          be sent upstream to the official spack repository, so most
          users will not need an overlay repo.

To select any of the non-default versions you can use the "@version" idiom::

     spack install siesta@5.0.2 +mpi +netcdf +libxc +elpa ^elpa@2024.05.001 +cuda cuda_arch=80

In all cases, the installation will contain all the utility programs
in the Siesta distribution.

Making the Spack-compiled versions available for use
----------------------------------------------------

After the "spack install" commands, the executables will be placed in
your spack area, but will not be immediately available in the
PATH. Installed packages might be queried with the "spack find"
command::

    spack find -lv siesta

The "-lv" options will offer a long-form output that includes the features selected in each executable.
    
In order to make the executables available you can run, for example::

    spack load siesta@5.0.2

It is also possible to use Spack's built-in `modulefiles generation mechanism
<https://spack.readthedocs.io/en/latest/module_file_support.html>`_. However, module naming is
not as fine-grained as the spec printed by `spack find -lv`, and it might be difficult to know
exactly which features a given module provides.


Use of Spack environments for compilation
-----------------------------------------

Spack environments can be profitably used to build consistent versions
of all the dependencies of a package, with particular options and
underlying system libraries.

Please see the spack documentation about environments. In a nutshell::

  mkdir my-siesta-env
  cd my-siesta-env
    #  (copy to this directory the spack.yaml file shown later)
  spack activate -p .
  spack install

An example `spack.yaml` file for an environment that will install CUDA-enabled Siesta is

.. literalinclude:: spack.yaml

The repo part at the end has to be filled in according to the instructions given in the previous sections.

The environment must be activated in order to use the installed executables (see the spack documentation)::

   spack activate -p /path/to/the/directory/my-siesta-env

This example deliberately uses an "unnamed" or "directory"
environment. Creation of "named" environments might need write access
to system directories in supercomputer center installations (refer to the note above).











.. _spack_issues:

Issues with System-Provided Spack Installations in Supercomputer Centers
------------------------------------------------------------------------

When using a “system-provided” Spack installation on shared
supercomputing environments, users often encounter restrictions that
prevent modifying or customizing the Spack configuration. These
restrictions can interfere with the normal process of adding
repositories, setting up custom configurations, or even overriding
default behavior for building and installing software packages. This
document highlights common issues and solutions to help you manage these
challenges effectively.

1. Configuration Restrictions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

System administrators in supercomputer centers often provide a
pre-installed Spack environment, which may have locked configurations.
In such setups, user-level customizations can be limited or disabled
altogether. Here are common issues:

-  **Locked Configuration Files**: The system may prevent users from
   modifying Spack configuration files like ``compilers.yaml``,
   ``packages.yaml``, and ``modules.yaml`` at the system level.

-  **Restricted Package Repositories**: Adding or prioritizing new
   repositories (such as the custom ``siesta-project-spack-repo``) may
   not be allowed by default.

-  **Read-only Spack installation**: System-provided Spack installations
   might be in read-only directories, preventing any changes to the
   Spack environment or its configuration.

2. Environment Variables to Control User Customization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Spack allows users to bypass some of these restrictions by adjusting the
behavior using environment variables:

a. ``SPACK_DISABLE_LOCAL_CONFIG``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The environment variable ``SPACK_DISABLE_LOCAL_CONFIG`` can be used to
prevent Spack from reading user-level configuration files that might
conflict with system settings (this is done, for example, in the LUMI
supercomputer). However, in certain cases, especially if
the system-provided Spack is highly constrained, you may **not** want to
disable local configuration because it’s your only avenue for
customization.

So if you need to apply user-specific settings, such as custom
repositories, package configurations, or compiler settings, check
whether ``SPACK_DISABLE_LOCAL_CONFIG`` is set. Unset it
(``unset SPACK_DISABLE_LOCAL_CONFIG``) or explicitly set it to
``FALSE``.

b. ``SPACK_USER_CONFIG_PATH``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The environment variable ``SPACK_USER_CONFIG_PATH`` allows you to set a
custom path where Spack should look for user-specific configuration
files. This can be helpful if you do not have write access to the
default configuration paths in the system-provided installation.

The system managers might have set this variable for you in a specific
area (this is the case in Leonardo). Otherwise, the default user
configuration path is ``~/.spack``. In the general case, you might want
to use an arbitrary path. The example below uses
``~/.spack-custom-config``:

-  **Usage**:

   -  Create a directory in your home folder (e.g.,
      ``~/.spack-custom-config``) and set the environment variable:

      .. code:: bash

         export SPACK_USER_CONFIG_PATH=~/.spack-custom-config

-  Populate this directory with your own versions of Spack configuration
   files, such as ``config.yaml``, ``packages.yaml``, or ``repos.yaml``.
   This enables you to maintain full control over your Spack environment
   despite system-level restrictions.

-  This is especially useful for adding custom repositories or adjusting
   package preferences without altering the system-provided Spack
   configuration.

3. Creating Custom Spack Configuration Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Even if you cannot modify the system Spack installation directly, you
can often override or supplement system defaults by creating your own
configuration files. These files allow you to customize the behavior of
Spack for your specific needs:

a. ``repos.yaml`` – Custom Repositories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To add the ``siesta-project-spack-repo`` repository to your Spack
environment, you can define a custom ``repos.yaml`` file in the
directory specified by ``SPACK_USER_CONFIG_PATH``.

1. Create a ``repos.yaml`` file under ``~/.spack-custom-config``:

   .. code:: yaml

      repos:
        - /path/to/your/custom/siesta-project-spack-repo
        - $spack/var/spack/repos/builtin

2. Ensure your custom repository is prioritized over the default
   ``builtin`` repository by listing it first. This ensures that Spack
   will use the custom Siesta recipes during the build process.

3. (If you are not using a ``repos.yaml`` file or the system prevents
   automatic addition) add the repository using the following command:

   .. code:: bash

      spack repo add /path/to/your/custom/siesta-project-spack-repo

b. ``packages.yaml`` – Package Customization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you want to customize certain package dependencies, such as
specifying preferred compilers, flags, or versions, you can create a
``packages.yaml`` file:

1. Example content for ``packages.yaml``:

   .. code:: yaml

      packages:
        mpi:
          paths:
            mpich: /path/to/mpich
          buildable: False
        hdf5:
          variants: +mpi

2. Save this file in the custom configuration path (e.g.,
   ``~/.spack-custom-config/packages.yaml``).

c. ``config.yaml`` – General Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``config.yaml`` file allows you to define broader settings for
Spack, such as build caches, the use of temporary directories, or where
Spack should store its builds.

Example configuration to specify installation and source cache
directories:

.. code:: yaml

   config:
     install_tree: /path/to/install/directory
     source_cache: /path/to/source/cache

4. Final Tips for Bypassing System Defaults
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  **Use Spack environments**: Spack environments (``spack env``) are
   another way to isolate and manage configurations. You can create
   environments that encapsulate your custom package repositories and
   configuration files, making it easier to manage software
   installations without conflicting with system settings.

-  **Create a local Spack installation**: If system restrictions are too
   limiting, consider installing your own local version of Spack. You
   can still leverage the system-provided Spack for basic modules and
   dependencies while customizing the rest of your environment with your
   personal Spack installation.

--------------

By following these steps, you can effectively manage a system-provided
Spack installation while maintaining control over your software
environment. This is particularly useful when installing custom software
like SIESTA that requires specialized package recipes or configurations.

.. _local_installation:

Setting up the local working environment for the tutorial exercises
===================================================================

In order to get the files for these tutorials, you first need to clone
this same documentation repository::

    git clone https://gitlab.com/siesta-project/documentation/siesta-docs.git

Afterwards execute the following::

    cd siesta-docs
    cd work-files
    bash link.sh

All needed files will now be under the ``work-files`` directory.

Compiling SIESTA
----------------

In case you need to compile your own version of SIESTA, check out
:ref:`this how-to<how-to-build-siesta>` section.


Additional things to download or install later
----------------------------------------------

Some items are not included in the standard SIESTA distribution
for both technical and licensing reasons. Examples include:

* Extra programs

  The programs to process the PDOS/PDOS.xml files can be obtained and
  compiled very easily. See :ref:`this how-to<how-to-dos-pdos>`.

* The FLOS library

  This is needed for the simulation code implemented in Lua
  (e.g. NEB, new relaxation algorithms, etc). It is easily installed
  following the instructions in :ref:`this how-to<how-to-flos>`.

* Other tutorial-specific material (such as extra packages and
  potentially large data files).

If needed, each tutorial will provide information about any
additional material needed.


.. note::
  Should you want to build your own version of this documentation for offline reading,
  you can do so using the files above (*siesta-docs*), minding the depedencies
  with ReadTheDocs Sphinx theme.
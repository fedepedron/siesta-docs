:sequential_nav: next

..  _tutorial-transiesta-c-chain:

Simple C-chain
--------------

The TranSIESTA tutorial is runned on a jupyter notebook. It processes with Python the results of SIESTA simulations.

The notebook can be downloaded by clicking :download:`here <work-files/TranSIESTA_tutorial.ipynb>`

This tutorial can be runned either on Google Colab, or on your local machine (provided it
is a Linux or Mac, for Windows you have to use WSL).

If you do not want to install stuff on your computer, please run it on `Google Colab <https://colab.research.google.com>`__.

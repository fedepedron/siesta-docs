#!/bin/bash

export OMP_NUM_THREADS=1

siesta_cmd="mpirun -n 4 siesta"
tbtrans_cmd="mpirun -n 4 tbtrans"
root=$(pwd)

echo Writing geometries
python ${root}/.execute_scripts/write_elec.py
python ${root}/.execute_scripts/write_unconnected.py
python ${root}/.execute_scripts/write_connected.py

# --------------------------------
#            Electrode
# --------------------------------

echo Executing electrode
cd ${root}/electrode
${siesta_cmd} < RUN.fdf > RUN.out

# --------------------------------
#            Unconnected
# --------------------------------

echo Executing unconnected/0V
cd ${root}/unconnected/0V
echo Unconnected 0V TranSIESTA
${siesta_cmd} < RUN.fdf > RUN.out
echo Unconnected 0V TBtrans
${tbtrans_cmd} < RUN.fdf > RUN_TBtrans.out

echo Copying from 0V to 1V
mkdir ${root}/unconnected/1V
cp ${root}/unconnected/0V/*.{psf,fdf,TSDE} ${root}/unconnected/1V
sed -i "/TS.Voltage/c\TS.Voltage 1.0 eV" ${root}/unconnected/1V/TS.fdf
cd ${root}/unconnected/1V

echo Unconnected 1V TranSIESTA
${siesta_cmd} < RUN.fdf > RUN.out
echo Unconnected 1V TBtrans
${tbtrans_cmd} < RUN.fdf > RUN_TBtrans.out


# --------------------------------
#            Connected
# --------------------------------

echo Executing connected/0V
cd ${root}/connected/0V
${siesta_cmd} < RUN.fdf > RUN.out

prevV=0

for V in 0.25 0.5 0.75 1; do
        echo Executing connected/${V}V

        # Create voltage directory
        mkdir ${root}/connected/${V}V
        cd ${root}/connected/${V}V

        # Copy density matrix, pseudopotentials and inputs from previous runs.
        cp ${root}/connected/${prevV}V/*.{TSDE,psf,fdf} ${root}/connected/${V}V

        # Update the voltage input in the TS.fdf file
        sed -i "/TS.Voltage/c\TS.Voltage ${V} eV" ${root}/connected/${V}V/TS.fdf

        # Run SIESTA
        ${siesta_cmd} < RUN.fdf  > RUN.out

        prevV=${V}
done

echo Running connected TBtrans
for V in $(seq 0 0.1 1) ; do

   # Get directory name
   d=TBT_${V//,/.}

   echo Executing Connected ${d}

   # Create the directory and enter
   mkdir ${root}/connected/${d}
   cd ${root}/connected/${d}

   # Copy all the input files from the 0V calculation
   cp ${root}/connected/0V/*fdf ${root}/connected/${d}
   # But overwrite the TBtrans.fdf with the input file containing
   # the options to compute only current.
   cp ${root}/connected/TBtrans_current.fdf ${root}/connected/${d}/TBtrans.fdf

   # Run TBtrans
   ${tbtrans_cmd} -V "${V//,/.}:eV" RUN.fdf > TBT.out

done

echo Done.

# Atomic structure is specified in device_coords.fdf
%include device_coords.fdf

# We use the default SZP basis set
PAO.BasisSize      SZP

# PBE functional
XC.functional      GGA
XC.authors         PBE

# K grid sampling of the Brillouin Zone. We run the calculation just
# on the Gamma point.
%block kgrid_Monkhorst_Pack
  1  0  0  0.0
  0  1  0  0.0
  0  0  1  0.0
%endblock kgrid_Monkhorst_Pack

# Fineness of the real space grid.
MeshCutoff          300.0 Ry

# Store the density matrix in a .DM file.
DM.UseSaveDM         true

# Some parameters to control the behavior of the SCF loop.
SCF.Mix              hamiltonian
MaxSCFIterations     200  
DM.MixingWeight      0.01
DM.NumberPulay       6
DM.Tolerance         9.d-4

# Include file containing TS options: electrodes, contours, etc...
%include TS.fdf
# Include file containing TBtrans options
%include TBtrans.fdf

# Flags to save information about charges and potentials. This instructs
# TranSIESTA to store these real space quantities in files, which sisl
# can then read to postprocess/plot.
SaveRho                      true
SaveDeltaRho                 true
SaveElectrostaticPotential   true
SaveTotalCharge              true
SaveTotalPotential           true
SaveIonicCharge              true

# Write partial atomic charges to the output file. We will
# read them with sisl to analyze them!
WriteHirshfeldPop true
WriteVoronoiPop true


root=$(pwd)

prevV=0

for V in 0.25 0.5 0.75 1; do
	echo Running V=${V} eV

	# Create voltage directory
        mkdir ${root}/${V}V
        cd ${root}/${V}V

	# Copy density matrix, pseudopotentials and inputs from previous runs.
        cp ${root}/${prevV}V/*.{TSDE,psf,fdf} ${root}/${V}V

	# Update the voltage input in the TS.fdf file
        sed -i "/TS.Voltage/c\TS.Voltage ${V} eV" ${root}/${V}V/TS.fdf
	
	# Run SIESTA
	siesta < RUN.fdf  > RUN.out
        
        prevV=${V}
done

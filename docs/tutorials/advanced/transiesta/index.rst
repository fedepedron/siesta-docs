:sequential_nav: next

..  _tutorial-transiesta:

TranSIESTA
----------

This is an introduction to TranSIESTA NEGF calculations.

.. hint::

   Additional tutorials may be found `here <https://github.com/zerothi/ts-tbt-sisl-tutorial>`__.

.. toctree::
   :maxdepth: 1
   :numbered:

   C_chain/index

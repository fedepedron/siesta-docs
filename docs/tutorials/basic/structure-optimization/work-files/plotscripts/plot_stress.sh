grep oig OUT | awk '{print NR, $5, $6, $7, $8, $9, $10}' >stress.data

gnuplot -persist <<-EOFMarker
set term png
set output "stress_iteration.png"
labels = "x y z yz xz xy"
set xlabel '# iteration'
set ylabel 'Stress tensor Voigt (kbar)'
plot for [col=2:7] 'stress.data' using 1:col  with lines title word(labels, col-1)
EOFMarker

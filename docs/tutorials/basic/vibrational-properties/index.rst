:sequential_nav: next

..  _tutorial-basic-vibrational-properties:

Vibration modes and phonons
===========================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this set of exercises we will use the method of finite-differences
implemented in Siesta to compute force constants in real space. We
will explore the cases of a crystal and a molecule. In the former
case we will focus on the need of a supercell to represent the
real-space force constants, while in the second we will understand
how to visualize the vibrational modes.

.. note::
    Phonon calculations can be very sensitive to numerical artifacts such as
    those derived from the :ref:`eggbox effect <tutorial-basic-grid-convergence>`.
    Be sure to have the rest of your simulation parameters already converged as best as you can
    before running the actual force constant matrix calculations.


.. toctree::
    :maxdepth: 1

    Si-bulk/index
    Benzene/index













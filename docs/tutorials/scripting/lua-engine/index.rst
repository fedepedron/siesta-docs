:sequential_nav: next

..  _tutorial-lua-engine:

The Lua scripting engine
========================

  For background on the use of Lua in Siesta, please see the `flos
  documentation site <https://flos.readthedocs.io/>`_

  In particular, you can read up on:

  * How hooks at certain points in the program are implemented
  * Which Siesta variables can be shared with Lua
  * The functionality implemented in the FLOS library.

A structural optimization example
---------------------------------

Here we just offer a glimpse of what be done with Lua in Siesta
through a practical example. We will take one of the :ref:`structural
relaxation<tutorial-basic-structure-optimization>` cases and run it
under the control of a Lua script that invokes the LBFGS algorithm.

.. hint::
   Enter the *relaxation* directory

Note the only two changes needed to enable the LBFGS optimizer::

  MD.TypeOfRun         Lua
  Lua.Script           relax_geometry_lbfgs.lua

The Lua script resides in the same directory.

In this case, the LBFGS algorithm appears marginally worse than the
Broyden method we used in the above-mentioned tutorial, but still much
better than 'conjugate gradients'. There are still some knobs to try
in the script, if you feel like it.

..
  A convenience example
  (This can be, for example, a script that will plot the eggbox-effect curve
  automatically.)





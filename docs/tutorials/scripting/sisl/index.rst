:sequential_nav: next

..  _tutorial-sisl-introduction:

Introduction to sisl
====================

You may not have heard of it yet, but there's a featureful python package which can help greatly improve your experience with SIESTA: `sisl <https://zerothi.github.io/sisl/docs/latest/index.html>`_.

We have created a jupyter notebook with the aim of **helping you understand the basics** of it so that you can **continue exploring on your own**.

You can find the files for the tutorial under *work-files/tutorials/basic/sisl*. 
There you will find a jupyter notebook and a folder called *SrTiO3*. This folder contains inputs to run a simulation. Please enter the directory and run
SIESTA, as we will need the output files of this simulation during the tutorial.

.. code-block:: 

    siesta < SrTiO3.fdf | tee SrTiO3.out

Once the simulation ends, we can proceed with the tutorial.
To open the notebook in the SIESTA mobile VM, you just need to make sure you are in the siesta_school environment:

.. code-block:: 

    workon siesta_school

Sisl's visualization dependencies are not installed in the environment, please do

.. code-block:: 

    pip install sisl[viz]

once you are in the siesta_school environment to make sure they get installed.

And then launch the jupyter notebook server (from the tutorial's directory):

.. code-block:: 

    jupyter notebook

You'll find a notebook called *Sisl_introduction.ipynb*, open it and follow the tutorial!
